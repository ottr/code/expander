package xyz.ottr.lutra.expander;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import picocli.CommandLine;
import picocli.CommandLine.ParameterException;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.rulesys.BuiltinRegistry;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.reasoner.rulesys.RuleReasoner;

@Command(
    name = "Lutra Expander",
    descriptionHeading = "%n@|bold DESCRIPTION:|@%n",
    parameterListHeading = "%n@|bold PARAMETERS:|@%n",
    optionListHeading = "%n@|bold OPTIONS:|@%n",
    footerHeading = "%n",
    description = "Simple program for expanding OTTR-templates using Jena-rules.",
    footer = "@|bold EXAMPLES:|@%n",
    mixinStandardHelpOptions = true
    //versionProvider = Settings.JarFileVersionProvider.class
)
public class Expander {

    @Option(names = {"-M", "--mode"},
        description = {"The mode to execute."})
    public String mode = "expand";

    @Option(names = {"-m", "--model"},
        description = {"The file with the model to use."})
    public String modelPath;

    @Option(names = {"-r", "--rules"},
        description = {"The rules to use for inference."})
    public String rulesPath;

    public static void main(String[] args) {

        Expander expander = new Expander();
        CommandLine cli = new CommandLine(expander);
        try {
            cli.parse(args);
        } catch (ParameterException ex) {
            System.err.println(ex.getMessage());
            return;
        }

        if (cli.isUsageHelpRequested()) {
            cli.usage(System.out);
        } else if (cli.isVersionHelpRequested()) {
            cli.printVersionHelp(System.out);
        } else {
            expander.execute();
        }
    }

    public void execute() {

        RuleReasoner reasoner = makeRuleReasoner();
        InfModel model = makeModel(reasoner);
        model.write(System.out, "TTL");
    }

    private RuleReasoner makeRuleReasoner() {

        BuiltinRegistry.theRegistry.register(new NotBuiltin());
        List<Rule> rules = null;

        if (this.mode.equals("custom")) {
            rules = Rule.rulesFromURL("file:" + rulesPath);
        } else {
            var in = Expander.class.getClassLoader().getResourceAsStream(this.mode + ".rules");
            if (in != null) {
                rules = Rule.parseRules(Rule.rulesParserFromReader(
                            new BufferedReader(new InputStreamReader(in))));
            } else {
                System.err.println("ERROR: Unknown mode " + this.mode);
                System.exit(1);
            }
        }

        return new GenericRuleReasoner(rules);
    }

    private InfModel makeModel(RuleReasoner reasoner) {

        Model model = ModelFactory.createDefaultModel();
        model.read(modelPath);
        return ModelFactory.createInfModel(reasoner, model);
    }
}
