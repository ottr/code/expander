package xyz.ottr.lutra.expander;

import java.util.HashMap;
import java.util.Map;
import org.apache.jena.graph.* ;
import org.apache.jena.reasoner.rulesys.* ;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin ;

public class NotBuiltin extends BaseBuiltin {

    @Override
    public String getName() {
        return "not";
    }

    @Override
    public boolean bodyCall(Node[] args, int length, RuleContext context) {

        if (length % 3 != 0 || length < 3) {
            throw new BuiltinException(this, context, "builtin " + getName() + " requires a number of arguments that is a multiple of 3 (and 3 or more), but saw " + length);
        }

        return !findMatch(new HashMap<>(), args, 0, context);
    }

    private boolean findMatch(Map<Node, Node> varMap, Node[] args, int pos, RuleContext context) {

        if (args.length <= pos) {
            return true; // All variables bound with match, and no more to bind
        }

        Node subj = getArgInContext(varMap, pos + 0, args, context);
        Node pred = getArgInContext(varMap, pos + 1, args, context);
        Node obj  = getArgInContext(varMap, pos + 2, args, context);
        
        var results = context.find(subj, pred, obj);
        if (!results.hasNext()) {
            return false; // No results, i.e. no match found with current bindings
        }

        while (results.hasNext()) {

            Map<Node, Node> newVarMap = new HashMap<>();
            newVarMap.putAll(varMap);

            Triple t = results.next();
            updateArgMap(subj, t.getSubject(),   newVarMap, args, pos + 0, context);
            updateArgMap(pred, t.getPredicate(), newVarMap, args, pos + 1, context);
            updateArgMap(obj,  t.getObject(),    newVarMap, args, pos + 2, context);

            if (findMatch(newVarMap, args, pos + 3, context)) {
                return true; // Match found
            }
        }
        return false; // No match found with current bindings
    }

    private Node getArgInContext(Map<Node, Node> varMap, int argPos, Node[] args, RuleContext context) {
        Node node = getArg(argPos, args, context);
        if (node.isVariable()) {
            node = varMap.getOrDefault(node, null);
        }
        return node;
    }

    private void updateArgMap(Node oldNode, Node newNode, Map<Node, Node> varMap, Node[] args, int argPos, RuleContext context) {
        if (oldNode == null) {
             varMap.put(getArg(argPos, args, context), newNode);
         }
    }

    @Override
    public boolean isMonotonic() {
        return false;
    }
}
